import React, { useState } from 'react';

import Table from './Components/Table';

const App = () => {
  const [nameList, setNameList] = useState(['John','Becky','George','Amy']);

  const [name, setName] = useState('');
// name validation functionality
  function nameValidation(){
   let isValid = false;
   for (let i=0;i<name.length;i++) {
      if ((name[i] >= 'A' && name[i]<= 'Z') || (name[i] >= 'a' && name[i] <='z')){
       isValid =true;
      }else{
        return false;
       }
   }
   return isValid;
  }
  
   const handleSubmitClick = () => {
    var isValid = nameValidation();
    if (isValid){
     setNameList(names => [...names, name]);
    }
    
   };
   const handleOnChange = (e)=>{ 
     setName(e.target.value)
 }

  return (
    <div className='formContainer'>
      <h1>Employee Names Table</h1>
      <label className='employee'>Add a new employee name:</label>  
      <input type='text' onChange={handleOnChange} />
      <button type="button" className='btn btn-outline-dark' value="add" onClick={handleSubmitClick}>Add</button>
      <Table names={nameList} />
    </div>
  );
}

export default App;
