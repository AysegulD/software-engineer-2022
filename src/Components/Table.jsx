import React, { useEffect, useState } from 'react';


const Table = ({ names }) => {
  
  const [sortedByNames, setSortedByNames] = useState([]);
  useEffect(() => {
    setSortedByNames(names);
  },[names]);

  const handleOnClick = () =>{  
    let names1 = sortedByNames;
    names1.sort((name1,name2) => {
      if (name1 === name2){
        return 0;
      }
      if(name1 > name2){
        return 1;
      }else return -1;
    });
    setSortedByNames([...names1]);
  }
  
  return (
    <>
    <div className="table-responsive">     
      <table className="table table-hover table-light">
        <thead className='table-dark'>
          <tr>
            <th>  
            {/* a button you can sort the table alphabetically */}
              <button type="button" className='btn btn-light' onClick={handleOnClick}>
              Employee Name             
              </button>  
            </th>
          </tr>
        </thead>
        <tbody>
          {sortedByNames?.map(name => (
          <tr >
            <td>{name}</td>
          </tr>
          ))}
        </tbody>
      </table>
    
    </div> 
    </>
  )
};

export default Table;
